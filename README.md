## Synopsis

This is a API minesweeper created in Django and Django Rest Framework.

---

## Endpoints

```
GET /api/v1/game/{{ GAME_ID }}/
```

* Gets a specific game




```
GET /api/v1/newgame/
```

* Create a new game, if you are logged, the game asociates with the user.

* Optionals Parameters:

> | Parameter     | Type          | Default       |
> | ------------- |:-------------:|:-------------:|
> | rows          | int           | 10            |
> | columns       | int           | 10            |
> | mines         | int           | 10            |




```
GET /api/v1/game/{{ GAME_ID }}/click/
```

* Click a specific cell in the game

* Parameters:

> | Parameter     | Type          |
> | ------------- |:-------------:|
> | row           | int           |
> | column        | int           |




```
GET /api/v1/game/{{ GAME_ID }}/flag/
```

* Flags a specific cell in the game

* Parameters:

> | Parameter     | Type          |
> | ------------- |:-------------:|
> | row           | int           |
> | column        | int           |




```
GET /api/v1/game/{{ GAME_ID }}/get_elapsed_time/
```

* Get the elapsed time of a specific game.




```
GET /api/v1/game/
```

* If you are logged, you can get a list of all your games.



```
GET /api-auth/login/
```

* You can log in here.



```
POST /api/accounts/
```

* Create new user

* Parameters:

> | Parameter     | Type          |
> | ------------- |:-------------:|
> | username      | string        |
> | password      | string        |
> | email         | string        |



---

## Installation on a clean ubuntu server
Install this requeriments:
```
sudo apt-get install python-pip python-dev build-essential libmysqlclient-dev apache2 libapache2-mod-wsgi
sudo apt-get install mysql-server
sudo pip install virtualenv 
```

use the apache configuration file in code /apache_conf.conf

Download the repo and run:

```
pip install -r requeriments.txt
```

Create a database, then edit the /minesweeper/settings.py file with the DB credentials.
