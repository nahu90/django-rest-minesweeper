# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models



class Game(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    grid = models.TextField(blank=True, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    columns = models.IntegerField(default=0, blank=True, null=True)
    rows = models.IntegerField(default=0, blank=True, null=True)
    mines = models.IntegerField(default=0, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "game_%s" % (self.id)
