"""minesweeper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from api import views
from web.views import test

router = routers.DefaultRouter()
router.register(r'accounts', views.UserView, 'list')

urlpatterns = [
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^api/v1/game/$', views.GameList.as_view(), name='game_list'),
    url(r'^accounts/profile/$', views.GameList.as_view(), name='game_list_profile'),
    url(r'^api/v1/game/(?P<pk>[0-9]+)/$', views.GameDetail.as_view(), name='game_detail'),
    url(r'^api/v1/game/(?P<pk>[0-9]+)/click/$', views.click, name='click'),
    url(r'^api/v1/game/(?P<pk>[0-9]+)/flag/$', views.flag, name='flag'),
    url(r'^api/v1/game/(?P<pk>[0-9]+)/get_elapsed_time/$', views.get_elapsed_time, name='elapsed_time'),
    url(r'^api/v1/newgame/$', views.new_game, name='newgame'),
  
    url(r'^test_game/(?P<game_id>[0-9]+)/$', test),
]
