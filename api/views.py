# -*- coding: utf-8 -*-
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth.models import User, Group
from rest_framework.response import Response
from rest_framework import routers, serializers, viewsets
from .permissions import IsStaffOrTargetUser
from rest_framework.views import APIView
from rest_framework import viewsets
from django.utils import timezone
from django.http import Http404
from api.serializers import *
from backend.models import *
from .services import *
import datetime
import random



class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    model = User
 
    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (AllowAny() if self.request.method == 'POST' else IsStaffOrTargetUser()),


class GameList(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        snippets = Game.objects.filter(user=request.user)
        serializer = GameSerializer(snippets, many=True)
        return Response({'objects': serializer.data})


class GameDetail(APIView):

    def get_object(self, pk):
        try:
            return Game.objects.get(pk=pk)
        except Game.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        game = self.get_object(pk)
        if game.user:
            if request.user.is_authenticated():
                if not game.user == request.user:
                    return Response({'errors':[{"message": "This game belongs to another user"}]})
            else:
                return Response({'errors':[{"message": "You must log in to play this game"}]})
        serializer = GameSerializer(game)
        return Response(serializer.data)


@api_view(['GET'])
def new_game(request):
    rows = int(request.GET.get('rows', 10))
    columns = int(request.GET.get('columns', 10))
    mines = int(request.GET.get('mines', 10))

    game = Game()
    game.start_time = timezone.now()
    game.columns = columns
    game.rows = rows
    game.mines = mines
    if request.user.is_authenticated():
        game.user = request.user
    game.save()

    grid = create_grid(game)
    grid = add_mines(grid, game)
    grid = add_numbers(grid, game)

    game.grid = json.dumps(grid)
    game.save()
    
    serializer = GameSerializer(game, context={'request': request})
    
    return Response(serializer.data)


@api_view(['GET'])
def click(request, pk):
    try:
        row = int(request.GET.get('row'))
        column = int(request.GET.get('column'))
    except:
        return Response("please send row and column as GET params")

    try:
        game = Game.objects.get(pk=pk)
    except Game.DoesNotExist:
        raise Http404

    if game.user:
        if request.user.is_authenticated():
            if not game.user == request.user:
                return Response({'errors':[{"message": "This game belongs to another user"}]})
        else:
            return Response({'errors':[{"message": "You must log in to play this game"}]})
        
    if game.active:
        grid = json.loads(game.grid)
        cell = grid[row][column]

        if cell['flag']:
            serializer = GameSerializer(game)
            return Response(serializer.data)
        elif cell['mine']:
            game.end_time = timezone.now()
            game.active = False
            grid = clear_grid(grid)
        elif cell['number'] > 0:
            grid[row][column]['clicked'] = True
        else:
            grid = clear_adjacent(grid, row, column, game)

        game.grid = json.dumps(grid)
        game.save()
        serializer = GameSerializer(game, context={'request': request})
        return Response(serializer.data)
    else:
        return Response({'errors':[{"message": "You can't click on a finished game"}]})


@api_view(['GET'])
def flag(request, pk):
    try:
        row = int(request.GET.get('row'))
        column = int(request.GET.get('column'))
    except:
        return Response({'errors':[{"message": "Please send row and column as GET params"}]})

    try:
        game = Game.objects.get(pk=pk)
    except Game.DoesNotExist:
        raise Http404

    if game.user:
        if request.user.is_authenticated():
            if not game.user == request.user:
                return Response({'errors':[{"message": "This game belongs to another user"}]})
        else:
            return Response({'errors':[{"message": "You must log in to play this game"}]})

    if game.active:
        grid = json.loads(game.grid)
        if not grid[row][column]['clicked']:
            if grid[row][column]['flag']:
                grid[row][column]['flag'] = False
            else:
                grid[row][column]['flag'] = True
        game.grid = json.dumps(grid)
        game.save()
        serializer = GameSerializer(game, context={'request': request})
        return Response(serializer.data)
    else:
        return Response({'errors':[{"message": "You can't flag on a finished game"}]})


@api_view(['GET'])
def get_elapsed_time(request, pk):
    try:
        game = Game.objects.get(pk=pk)
    except Game.DoesNotExist:
        raise Http404

    if game.user:
        if request.user.is_authenticated():
            if not game.user == request.user:
                return Response({'errors':[{"message": "This game belongs to another user"}]})
        else:
            return Response({'errors':[{"message": "You must log in to play this game"}]})

    if game.active:
        c_date = timezone.now() - game.start_time
    else:
        c_date = game.end_time - game.start_time

    return Response({'elapsed_time':"%02d:%02d" % divmod(c_date.days * 86400 + c_date.seconds, 60)})



