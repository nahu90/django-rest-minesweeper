# -*- coding: utf-8 -*-
import datetime
import random

def create_grid(game):
    return [[{'mine': False, 'number': 0, 'flag': False, 'clicked': False } for i in range(game.columns)] for i in range(game.rows)]

def generate_random_cell(game):
    random_row = random.randint(0, game.rows - 1)
    random_column = random.randint(0, game.columns - 1)

    return random_row, random_column

def add_mines(grid, game):
    for i in range(game.mines):
        correct_mine = False
        while not correct_mine:
            random_row, random_column = generate_random_cell(game)
            if not grid[random_row][random_column]['mine']:
                grid[random_row][random_column]['mine'] = True
                correct_mine = True
    return grid

def add_numbers(grid, game):
    for rowno, row in enumerate(grid):
        for colno, cell in enumerate(row):
            if cell['mine']:
                for i in range(-1, 2):
                    for j in range(-1, 2):
                        if i == 0 and j == 0:
                            continue
                        elif -1 < (rowno + j) < game.rows and -1 < (colno + i) < game.columns:
                            grid[rowno + j][colno + i]['number'] += 1 
    return grid
  
def clear_grid(grid):
    for rowno, row in enumerate(grid):
        for colno, cell in enumerate(row):
            grid[rowno][colno]['clicked'] = True
    return grid

def clear_block(grid, rowno, colno, game):
    for j in range(-1, 2):
        for i in range(-1, 2):
            actual_row = rowno + j
            actual_column = colno + i
            if -1 < (actual_row) < game.rows and -1 < (actual_column) < game.columns:
                grid[actual_row][actual_column]['clicked'] = True
    return grid

def get_zeros(grid, rowno, colno, game):
    zero_cells = []
    for j in range(-1, 2):
        for i in range(-1, 2):
            actual_row = rowno + j
            actual_column = colno + i
            if -1 < (actual_row) < game.rows and -1 < (actual_column) < game.columns:
                if i != 0 and j != 0:
                    pass
                elif grid[actual_row][actual_column]['number'] == 0:
                    zero_cells.append((actual_row, actual_column))
    return zero_cells

def append_zeros(zeros_to_check, checked_zeros):
    for zero_to_check in zeros_to_check:
        if zero_to_check not in checked_zeros:
            checked_zeros.append(zero_to_check)
    return checked_zeros

def clear_adjacent(grid, rowno, colno, game):
    checked_zeros = [(rowno, colno), ]
    zeros_to_check = get_zeros(grid, rowno, colno, game)

    while zeros_to_check:
        new_zeros_to_check = []
        for zero_to_check in zeros_to_check:
            row, col = zero_to_check
            result_zeros = get_zeros(grid, row, col, game)
            checked_zeros = append_zeros([(row, col), ], checked_zeros)

            for result_zero in result_zeros:
                if result_zero not in new_zeros_to_check and result_zero not in checked_zeros and result_zero not in zeros_to_check:
                    new_zeros_to_check.append(result_zero)
        zeros_to_check = new_zeros_to_check

    for checked_zero in checked_zeros:
        row, col = checked_zero
        grid = clear_block(grid, int(row), int(col), game)

    return grid





















