# -*- coding: utf-8 -*-
from __future__ import division, unicode_literals
from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework import fields
from django.utils import timezone
from backend.models import *
import json


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

class GameSerializer(serializers.HyperlinkedModelSerializer):
    grid = serializers.SerializerMethodField('get_grid_')
    elapsed_time = serializers.SerializerMethodField('get_elapsed_time_')
    user = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    def get_grid_(self, st):
        return json.loads(st.grid)

    def get_elapsed_time_(self, st):
        if st.active:
            c_date = timezone.now() - st.start_time
        else:
            c_date = st.end_time - st.start_time
        return "%02d:%02d" % divmod(c_date.days * 86400 + c_date.seconds, 60)

    class Meta:
        model = Game
        fields = ('id', 'user', 'start_time', 'end_time', 'active', 'columns', 'rows', 'mines', 'elapsed_time', 'grid')
