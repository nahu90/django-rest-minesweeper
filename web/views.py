# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.shortcuts import render
from backend.models import Game
import json



def test(request, game_id):
    c = {}
    game = Game.objects.get(pk=game_id)
    c['game'] = game
    c['grid'] = json.loads(game.grid)
    return render_to_response('test.html', c, RequestContext(request))



